## System Architecture

Browser -- WebHost -- LoadBalancer -- Server -- Datastore

* WebHost (Nginx) acting as React App host
* LoadBalancer (Nginx) acting load-balancer for Server
* Server (Nodejs) acting as real-time server for React App using Socket.io
* Datastore (Redis) used to cache currency price data retrieved from Cryptonator API

## Consideration of Requirements

* Socket.io used to ensure React app price data is updated by server in real-time
* Bootstrap components/css used to create responsive React App
* Server (Nodejs) tier can be scaled to multiple containers
* Nginx used as load balancer between Nodejs containers as system scales
* One instance of Server is responsible for getting data from Cryptonator to minimise API calls
* Redis used to cache currency price data to minimise Cryptonator API calls
* Docker and docker-compose used to dockerise application and control system start-up/shut-down/scaling

## Installation
```cmd
mkdir crypto-price
cd crypto-price
git clone git@bitbucket.org:jkscheung/cryptopriceserver.git server
git clone git@bitbucket.org:jkscheung/cryptopriceapp.git app
git clone git@bitbucket.org:jkscheung/cryptopriceconfig.git config
```
You should end up with the following directory structure.
```
.
+-- crypto-price
|   +-- app
|   +-- config
|   +-- server
```

### Build the Frontend
```cmd
cd crypto-price/app
yarn build
```

### Build the Docker Containers
```cmd
cd crypto-price/config
docker-compose build
```

## Operations

### Start Up
```cmd
cd crypto-price/config
docker-compose up
```
The system will start the containers in the required order.
Point your web browser to [localhost](http://localhost). You should now be able to see the live prices. The console will output debug messages.

### Shut Down
```cmd
cd crypto-price/config
docker-compose down
```
The running containers will be shut down.

### Scaling Up
```cmd
cd crypto-price/config
docker-compose scale server=3
```
2 more server containers will be started. Nginx will reload config to load-balance to the new containers.

### Scaling Down
```cmd
cd crypto-price/config
docker-compose scale server=1
```
The additional server containers will be shut down. Nginx will reload config to reflect changes.
